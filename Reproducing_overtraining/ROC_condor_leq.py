"""Produce roc curves from tagger output and labels."""
from __future__ import annotations

from h5py import File
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from ftag import get_discriminant

from puma import Roc, RocPlot
from puma.hlplots import Results, Tagger
from puma.metrics import calc_rej
from puma.utils import get_dummy_2_taggers, logger
from ftag import Cuts
from ftag import Flavours
from ftag.hdf5 import H5Reader
from pathlib import Path
import time
import sys

#input_file = "/eos/user/b/bricci/ntuples/h5/zprime.h5"
input_file = "/eos/user/b/bricci/ntuples/h5/newttbar.h5"

tagger = 'GN2v00'
flavours = {'b': 5, 'c': 4, 'u': 0}

trk = int(sys.argv[1])

batch_size=10000
column_names = ["GN2v00_pb", "GN2v00_pc", "GN2v00_pu", "HadronConeExclTruthLabelID", "eventNumber", "pt", "eta", "n_tracks"]
reader = H5Reader(input_file, batch_size=batch_size)
start_time = time.time()
jets = reader.load({"jets": ["GN2v00_pb",
                "GN2v00_pc",
                "GN2v00_pu",
                "HadronConeExclTruthLabelID",
                "eventNumber",
                "pt",
                "eta",
                "n_tracks"
                ]},
                #num_jets=100000, #229437218
                cuts=Cuts.from_list(["pt > 20e3","pt < 250e3", "eta < 2.5","eta > -2.5"])
                #cuts=Cuts.from_list(["pt > 250e3","pt < 6000e3", "eta < 2.5","eta > -2.5"])
                )

print(reader.num_jets)



end_time = time.time()


elapsed_time = end_time - start_time
minutes = elapsed_time // 60
seconds = elapsed_time % 60
print("Time: {} min {} sec".format(int(minutes), int(seconds)))

# Convert jets to DataFrame
#jets = pd.DataFrame(jets,columns=column_names)

# Concatenate all dataframes
#jets = pd.concat(dataframes)
#print(jets.head())
#print(jets["jets"].dtype)
#print(jets["jets"].dtype.names)

#print(jets.keys())
#print("jets type: ",type(jets))
len_jets=len(jets['jets'])
#print(len_jets)
#print("jets['HadronConeExclTruthLabelID'] type: ",type(jets['HadronConeExclTruthLabelID']))
print(f"Running with track leq: {trk}")
#########################################


select_train = ((jets["jets"]['eventNumber'] % 10 <= 7)& (jets["jets"]['n_tracks'] <= trk))
select_val = ((jets["jets"]['eventNumber'] % 10 <= 8)& (jets["jets"]['n_tracks'] <= trk))
select_test = ((jets["jets"]['eventNumber'] % 10 <= 9)& (jets["jets"]['n_tracks'] <= trk))
track_cut=(jets["jets"]['n_tracks'] <= trk)
number_jets = np.sum(track_cut)
print("number_jets tracks leq {}: ".format(trk),number_jets)

select_c = (jets["jets"]['HadronConeExclTruthLabelID']== flavours['c'])
select_b = (jets["jets"]['HadronConeExclTruthLabelID']== flavours['b'])
select_u = (jets["jets"]['HadronConeExclTruthLabelID']== flavours['u'])
# prima calcola la selezione per il training
jets_train = jets["jets"][select_train]
# poi calcola le selezioni per b e u sui dati di training
select_c_train = (jets_train['HadronConeExclTruthLabelID'] == flavours['c'])
select_b_train = (jets_train['HadronConeExclTruthLabelID'] == flavours['b'])
select_u_train = (jets_train['HadronConeExclTruthLabelID'] == flavours['u'])

# prima calcola la selezione per il validation
jets_val = jets["jets"][select_val]
# poi calcola le selezioni per b e u sui dati di validation
select_c_val = (jets_val['HadronConeExclTruthLabelID'] == flavours['c'])
select_b_val = (jets_val['HadronConeExclTruthLabelID'] == flavours['b'])
select_u_val = (jets_val['HadronConeExclTruthLabelID'] == flavours['u'])

# prima calcola la selezione per il testing
jets_test = jets["jets"][select_test]
# poi calcola le selezioni per b e u sui dati di testing
select_c_test = (jets_test['HadronConeExclTruthLabelID'] == flavours['c'])
select_b_test = (jets_test['HadronConeExclTruthLabelID'] == flavours['b'])
select_u_test = (jets_test['HadronConeExclTruthLabelID'] == flavours['u'])


#calculate discriminant
logger.info("caclulate tagger discriminants")
discs_GN2v00_train = get_discriminant(jets_train, "GN2v00", signal="bjets",fc=0.018)
discs_GN2v00_val = get_discriminant(jets_val, "GN2v00", signal="bjets",fc=0.018)
discs_GN2v00_test = get_discriminant(jets_test, "GN2v00", signal="bjets",fc=0.018)

# defining target efficiency
sig_eff = np.linspace(0.49, 1, 20)
# defining boolean arrays to select the different flavour classes

n_jets_light = sum(select_u)
n_jets_c = sum(select_c)
n_jets_b = sum(select_b)

logger.info("Calculate rejection")
train_ujets_rej = calc_rej(discs_GN2v00_train[select_b_train], discs_GN2v00_train[select_u_train], sig_eff)
train_cjets_rej = calc_rej(discs_GN2v00_train[select_b_train], discs_GN2v00_train[select_c_train], sig_eff)

val_ujets_rej = calc_rej(discs_GN2v00_val[select_b_val], discs_GN2v00_val[select_u_val], sig_eff)
val_cjets_rej = calc_rej(discs_GN2v00_val[select_b_val], discs_GN2v00_val[select_c_val], sig_eff)

test_ujets_rej = calc_rej(discs_GN2v00_test[select_b_test], discs_GN2v00_test[select_u_test], sig_eff)
test_cjets_rej = calc_rej(discs_GN2v00_test[select_b_test], discs_GN2v00_test[select_c_test], sig_eff)

####################
n_jets_light_train = sum(select_u_train)
n_jets_c_train = sum(select_c_train)
n_jets_b_train = sum(select_b_train)
n_jets_light_test = sum(select_u_test)
n_jets_c_test = sum(select_c_test)
n_jets_b_test = sum(select_b_test)
n_jets_light_val = sum(select_u_val)
n_jets_c_val = sum(select_c_val)
n_jets_b_val = sum(select_b_val)



# here the plotting of the roc starts
logger.info("Plotting ROC curves.")

#atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $Z'$ MC23 \n 250<$p_{{T}}$<6000 GeV, |$\eta$|<2.5 \n #tracks $\leq$ {}, #jets:{} ".format(trk,number_jets)
atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $t\\bar{{t}}$ MC23 \n 20<$p_{{T}}$<250 GeV, |$\eta$|<2.5 \n #tracks $\leq$ {}, #jets:{} ".format(trk,number_jets)
plot_roc = RocPlot(
    n_ratio_panels=2,
    ylabel="Background rejection",
    xlabel="$b$-jet efficiency",
    #atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $t\\bar{t}$ MC23 \n# jets: 25000000",
    #atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $Z'$ run3 \n# jets: 300000",
    #atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $t\\bar{t}$ MC23 \n 20<$p_{{T}}$<250 GeV, |$\eta$|<2.5",
    atlas_second_tag=atlas_second_tag,#atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $Z'$ MC23 \n# jets: 25000000, #tracks $\leq 4$ \n 250<$p_{{T}}$<6000 GeV, |$\eta$|<2.5",
    #atlas_second_tag="$\\sqrt{{s}}=13$ TeV, $Z'$ MC23 \n # tracks $\geq$2 \n 250<$p_{{T}}$<5000 GeV, |$\eta$|<2.5",
    figsize=(6.5, 6),
    y_scale=1.4,
    xmin=0.6,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        train_ujets_rej,
        n_test=n_jets_light_train,
        rej_class="ujets",
        signal_class="bjets",
        label="GN2v00 train",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        test_ujets_rej,
        n_test=n_jets_light_test,
        rej_class="ujets",
        signal_class="bjets",
        label="GN2v00 test",
    ),
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        val_ujets_rej,
        n_test=n_jets_light_val,
        rej_class="ujets",
        signal_class="bjets",
        label="GN2v00 val",
    ),
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        train_cjets_rej,
        n_test=n_jets_c_train,
        rej_class="cjets",
        signal_class="bjets",
        label="GN2v00 train",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        test_cjets_rej,
        n_test=n_jets_c_test,
        rej_class="cjets",
        signal_class="bjets",
        label="GN2v00 test",
    ),
)

plot_roc.add_roc(
    Roc(
        sig_eff,
        val_cjets_rej,
        n_test=n_jets_c_val,
        rej_class="cjets",
        signal_class="bjets",
        label="GN2v00 val",
    ),
)
# setting which flavour rejection ratio is drawn in which ratio panel
plot_roc.set_ratio_class(1, "ujets")
plot_roc.set_ratio_class(2, "cjets")

plot_roc.draw()
#plot_roc.savefig("zprime_cuts.png", transparent=False)
#plot_roc.savefig("zprime_leq{}.png".format(trk), transparent=False)
plot_roc.savefig("ttbar_06leq{}.png".format(trk), transparent=False)
#plot_roc.savefig("zprime_condor_test.png".format(trk), transparent=False)


